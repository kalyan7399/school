package com.mindtree.app.service;

import java.util.Map;

import com.mindtree.app.entity.Doctor;
import com.mindtree.app.entity.Patient;
import com.mindtree.app.exception.ServiceException;

public interface HospitalService 
{
	public Map<Integer,Doctor> displayAllDoctors()throws ServiceException;
	
	public Map<Integer, Patient> displayAllPatients() throws ServiceException;
	
	public Doctor addDoctor(Doctor doctor) throws ServiceException;
	
	public Patient addPatient(Patient patient) throws ServiceException;
	
	public Patient assignDoctorToPatient(int patientid,int doctorid) throws ServiceException;
	
	public Map<Integer, Patient> displayParticularPatient(int patientid) throws ServiceException;
	
	public Map<Integer, Doctor> displayParticularDoctor(int doctorid) throws ServiceException;
	public Map<Integer,Doctor> updateParticularDoctor(int doctorid) throws ServiceException;

	public Doctor updateDoctor(int id, int exp);

	public void deletePatient(int id);
	
}

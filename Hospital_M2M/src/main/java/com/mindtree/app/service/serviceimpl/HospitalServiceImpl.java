package com.mindtree.app.service.serviceimpl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.mindtree.app.entity.Doctor;
import com.mindtree.app.entity.Patient;
import com.mindtree.app.exception.DoctorNotFoundException;
import com.mindtree.app.exception.DuplicateDoctorFoundException;
import com.mindtree.app.exception.DuplicatePatientFoundException;
import com.mindtree.app.exception.PatientNotFoundException;
import com.mindtree.app.exception.ServiceException;
import com.mindtree.app.repository.DoctorRepository;
import com.mindtree.app.repository.PatientRepository;
import com.mindtree.app.service.HospitalService;

@Service
public class HospitalServiceImpl implements HospitalService
{
	@Autowired
	DoctorRepository doctorRepo;
	@Autowired
	PatientRepository patientRepo;
	@Override
	public Map<Integer, Doctor> displayAllDoctors() throws ServiceException 
	{
		List<Doctor> doctorList=doctorRepo.findAllDoctors();
		Map<Integer,Doctor> doctorMap=new HashMap<Integer,Doctor>();
		for(Doctor data:doctorList)
		{
			Doctor doctor=doctorRepo.getById(data.getDoctorId());
			doctorMap.put(data.getDoctorId(), doctor);
		}
		if(doctorList.isEmpty())
		{
			throw new DoctorNotFoundException("No Doctor Found");
		}
		return doctorMap;
	}
	@Cacheable(value="displayAllPatients")
	public Map<Integer, Patient> displayAllPatients() throws ServiceException 
	{
		List<Patient> patientList=patientRepo.findAll();
		Map<Integer,Patient> patientMap=new HashMap<Integer,Patient>();
		for(Patient data:patientList)
		{
			Patient patient=patientRepo.getById(data.getPatientId());
			patientMap.put(data.getPatientId(), patient);
			try 
			{
				Thread.sleep(5000);
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
		}
		if(patientList.isEmpty())
		{
			throw new PatientNotFoundException("No Patient Found");
		}
		return patientMap;
	}

	@Override
	public Doctor addDoctor(Doctor doctor) throws ServiceException 
	{	
		List<Doctor> data=doctorRepo.findAll();
		for(int j=0;j<data.size();j++) 
		{
			if(doctor.getName().equals(data.get(j).getName())) 
			{
				throw new DuplicateDoctorFoundException("Duplicate Entry Found");
			}
		}
		return doctorRepo.save(doctor);
	}

	@Override
	public Patient addPatient(Patient patient) throws ServiceException 
	{	
		List<Patient> data=patientRepo.findAll();
		for(int j=0;j<data.size();j++) 
		{
			if(patient.getName().equals(data.get(j).getName())) 
			{
				throw new DuplicatePatientFoundException("Duplicate Entry Found");
			}
		}
		return patientRepo.save(patient);
	}

	@Override
	public Patient assignDoctorToPatient(int patientid, int doctorid) throws ServiceException 
	{	
			Patient pat=patientRepo.findById(patientid).get();
			Set<Doctor> doctorList=new HashSet<Doctor>();
			Doctor doc=doctorRepo.findById(doctorid).get();
			doctorList.add(doc);
			doctorRepo.setDoctor(patientid, doctorid);
			return patientRepo.save(pat);
	}

	@Override
	public Map<Integer, Patient> displayParticularPatient(int patientid) throws ServiceException 
	{	
		Optional<Patient> pat=patientRepo.findById(patientid);
		pat.orElseThrow(() -> new PatientNotFoundException("Patient Not Found"));
		Patient patient=patientRepo.findById(patientid).get();
		Map<Integer,Patient> patientMap=new HashMap<Integer,Patient>();
		patientMap.put(patient.getPatientId(), patient);

		return patientMap;
		
		
	}

	@Override
	public Map<Integer, Doctor> displayParticularDoctor(int doctorid) throws ServiceException 
	{
		Optional<Doctor> pat=doctorRepo.findById(doctorid);
		pat.orElseThrow(() -> new DoctorNotFoundException("Doctor Not Found"));
		Doctor doctor=doctorRepo.findById(doctorid).get();
		Map<Integer,Doctor> doctorMap=new HashMap<Integer,Doctor>();
		doctorMap.put(doctor.getDoctorId(), doctor);

		return doctorMap;
	}
	@Override
	public Map<Integer, Doctor> updateParticularDoctor(int doctorid) throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Doctor updateDoctor(int id, int exp) {
		Optional<Doctor> optional = doctorRepo.findById(id);
		Doctor doctor = optional.get();
		doctor.setExperience(exp);
		doctorRepo.save(doctor);
		return doctor;
	}
	@Override
	public void deletePatient(int id) {
		patientRepo.deleteById(id);
	}
	}
	


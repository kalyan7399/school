package com.mindtree.app.exception;

public class DuplicateDoctorFoundException extends ServiceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DuplicateDoctorFoundException() {
		// TODO Auto-generated constructor stub
	}

	public DuplicateDoctorFoundException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public DuplicateDoctorFoundException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public DuplicateDoctorFoundException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public DuplicateDoctorFoundException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

}

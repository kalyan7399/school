package com.mindtree.app.exception;

public class DuplicatePatientFoundException extends ServiceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DuplicatePatientFoundException() {
		// TODO Auto-generated constructor stub
	}

	public DuplicatePatientFoundException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public DuplicatePatientFoundException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public DuplicatePatientFoundException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public DuplicatePatientFoundException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

}

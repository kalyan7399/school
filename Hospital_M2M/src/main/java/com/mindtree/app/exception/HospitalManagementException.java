package com.mindtree.app.exception;

public class HospitalManagementException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4818225151406059304L;

	public HospitalManagementException() {
		// TODO Auto-generated constructor stub
	}

	public HospitalManagementException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public HospitalManagementException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public HospitalManagementException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public HospitalManagementException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

}

package com.mindtree.app.exception;

public class DoctorNotFoundException extends ServiceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DoctorNotFoundException() {
		// TODO Auto-generated constructor stub
	}

	public DoctorNotFoundException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public DoctorNotFoundException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public DoctorNotFoundException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public DoctorNotFoundException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

}

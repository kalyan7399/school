package com.mindtree.app.entity;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
public class Patient 
{
	@Id
	@GeneratedValue
	private int patientId;
	
	@NotNull(message="name is required in this field")
	@Size(min=3,max=60,message="first name should be betweem 3 and 60 characters")
	private String name;
	
	@NotNull(message="age required in this field")
	private int age;
	
	@NotNull(message="address is required in this field")
	@Size(min=3,max=60,message="first name should be betweem 3 and 60 characters")
	private String address;
	
	@NotNull(message="date of consultation is required in this field")
	@Size(min=3,max=60,message="first name should be betweem 3 and 60 characters")
	private String dateOfConsultation;
	@ManyToMany
	private Set<Doctor> doctor;
	
	public Patient() {
		super();
	}

	public int getPatientId() {
		return patientId;
	}

	public void setPatientId(int patientId) {
		this.patientId= patientId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDateOfConsultation() {
		return dateOfConsultation;
	}

	public void setDateOfConsultation(String dateOfConsultation) {
		this.dateOfConsultation = dateOfConsultation;
	}

	public Set<Doctor> getDoctor() {
		return doctor;
	}

	public void setDoctor(Set<Doctor> doctor) {
		this.doctor = doctor;
	}

	@Override
	public String toString() {
		return "Patient [patientId=" + patientId + ", name=" + name + ", age=" + age + ", address=" + address
				+ ", dateOfConsultation=" + dateOfConsultation + ", doctor=" + doctor + "]";
	}
	
	
}


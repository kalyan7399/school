package com.mindtree.app.entity;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
public class Doctor 
{
	@Id
	@GeneratedValue
	private int doctorId;
	
	@NotNull(message="name is required in this field")
	@Size(min=3,max=60,message="first name should be betweem 3 and 60 characters")
	private String name;
	
	@NotNull(message="experience field is required")
	private int experience;
	
	@NotNull(message=" gender field is required")
	@Size(min=3,max=60,message="gender is required")
	private String gender;
	
	@NotNull(message="specializationfield is required")
	@Size(min=3,max=60,message="speciliazation is required")
	private String specialization;
	@ManyToMany
	@JsonBackReference
	private Set<Patient> patient;
	public Doctor() 
	{
		super();
	}
	public int getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(int doctorId) {
		this.doctorId = doctorId;
	}

	
	public Set<Patient> getPatient() {
		return patient;
	}
	public void setPatient(Set<Patient> patient) {
		this.patient = patient;
	}
	public int getExperience() {
		return experience;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public int experience() {
		return experience;
	}

	public void setExperience(int experience) {
		this.experience = experience;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getSpecialization() {
		return specialization;
	}

	public void setSpecialization(String specialization) {
		this.specialization = specialization;
	}
	@Override
	public String toString() {
		return "Doctor [doctorId=" + doctorId + ", name=" + name + ", experience=" + experience + ", gender=" + gender
				+ ", specialization=" + specialization + ", patient=" + patient + "]";
	}
}

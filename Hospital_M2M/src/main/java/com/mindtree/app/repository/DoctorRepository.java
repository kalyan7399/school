package com.mindtree.app.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mindtree.app.entity.Doctor;

@Repository
public interface DoctorRepository extends JpaRepository<Doctor,Integer>
{
	@Transactional
	@Modifying
    @Query(value = "CALL `manytomany`.`assignDoctor`(?,?);", nativeQuery = true)
	void setDoctor(int patientId,int doctorId);

	
	@Query("from Doctor")
	List<Doctor> findAllDoctors();
	
}

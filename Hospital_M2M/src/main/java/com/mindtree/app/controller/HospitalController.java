package com.mindtree.app.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mindtree.app.entity.Doctor;
import com.mindtree.app.entity.Patient;
import com.mindtree.app.exception.HospitalManagementException;
import com.mindtree.app.exception.ServiceException;
import com.mindtree.app.service.HospitalService;

@RestController
public class HospitalController 
{
	@Autowired
	HospitalService service;
	
	private Map<String,Object> response;
	
	@GetMapping("/allDoctors")
	public ResponseEntity<Map<String, Object>> allDoctors() throws HospitalManagementException
	{
		response = new HashMap<String,Object>();
		response.put("message", "Getting Doctors succesfully");
		response.put("status",HttpStatus.OK);
		response.put("body", service.displayAllDoctors());
		response.put("error", false);
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
	}
	
	@GetMapping("/allPatients")               //getting all the patients
	public ResponseEntity<Map<String, Object>> allPatients() throws HospitalManagementException
	{
		response = new HashMap<String,Object>();
		response.put("message", "Getting Patients succesfully");
		response.put("status",HttpStatus.OK);
		response.put("body", service.displayAllPatients());
		response.put("error", false);
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
	}
	@PostMapping("/addDoctor")  //adding doctor details
	public ResponseEntity<Map<String, Object>> addDoctor(@RequestBody Doctor doctor) throws HospitalManagementException
	{
		response = new HashMap<String,Object>();
		response.put("message", "Doctor Added succesfully");
		response.put("status",HttpStatus.OK);
		response.put("body", service.addDoctor(doctor));
		response.put("error", false);
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
	}
	@PostMapping("/addPatient")      //adding patient details
	public ResponseEntity<Map<String, Object>> addPatient(@RequestBody Patient patient) throws HospitalManagementException
	{
		response = new HashMap<String,Object>();
		response.put("message", "Patient Added succesfully");
		response.put("status",HttpStatus.OK);
		response.put("body", service.addPatient(patient));
		response.put("error", false);
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
	}
	@PostMapping("/assignDoctor/{patientid}/{doctorid}") //assigning doctor based on id
	public ResponseEntity<Map<String, Object>> addPatient(@PathVariable("patientid") int patientid,@PathVariable("doctorid") int doctorid) throws HospitalManagementException
	{
		response = new HashMap<String,Object>();
		response.put("message", "Doctor Assigned to Patient succesfully");
		response.put("status",HttpStatus.OK);
		response.put("body", service.assignDoctorToPatient(patientid, doctorid));
		response.put("error", false);
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
	}
	@GetMapping("/doctor/{doctorid}")   //getting doctor details based on id
	public ResponseEntity<Map<String, Object>> displayParticularDoctor(@PathVariable("doctorid") int doctorid) throws HospitalManagementException
	{
		response = new HashMap<String,Object>();
		response.put("message", "Doctor Found ! ");
		response.put("status",HttpStatus.OK);
		response.put("body", service.displayParticularDoctor(doctorid));
		response.put("error", false);
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
	}
	@GetMapping("/patient/{patientid}")     //getting patient by patient id
	public ResponseEntity<Map<String, Object>> displayParticularPatient(@PathVariable("patientid") int patientid) throws HospitalManagementException
	{
		response = new HashMap<String,Object>();
		response.put("message", "Patient Found ! ");
		response.put("status",HttpStatus.OK);
		response.put("body", service.displayParticularPatient(patientid));
		response.put("error", false);
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
	}
	@PutMapping("updateDoctor/{doctorId}/{experience}")    //updating doctor experience by id
	public Doctor updateExperience(@PathVariable("doctorId") int id, @PathVariable("experience") int exp) {
		Doctor doctor = service.updateDoctor(id,exp);
		
		return doctor;
		
	}
	@DeleteMapping("/deletePatient/{patientId}")
	public String deletePatient(@PathVariable("patientId") int id) {
		service.deletePatient(id);
		return "deleted";
	}


}

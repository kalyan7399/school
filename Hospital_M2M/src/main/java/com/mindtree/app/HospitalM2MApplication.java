package com.mindtree.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class HospitalM2MApplication 
{
	public static void main(String[] args) 
	{
		SpringApplication.run(HospitalM2MApplication.class, args);
	}
}

package com.mindtree.app.handler;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.mindtree.app.exception.HospitalManagementException;
@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler 
{

Map<String,Object> response;
	
	@ExceptionHandler(value= {HospitalManagementException.class})
	protected ResponseEntity<Map<String,Object>> 
	handleConflict(HospitalManagementException ex,WebRequest request)
	{
		response = new HashMap<String,Object>();
		response.put("message",ex.getMessage());
	response.put("Status",HttpStatus.NOT_FOUND);
	response.put("body",null);
        	response.put("error",true);
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
	}

}
